use chrono::{
    NaiveDateTime,
    DateTime,
    Local,
    Utc,
    Offset
};

use std::io::{
    BufReader,
    BufRead,
};

use std::convert::{
    TryFrom,
    TryInto,
};

#[derive(Debug)]
pub struct Event {
    pub summary: String,
    pub description: String,
    pub location: String,
    pub dtstart: DateTime<Local>,
    pub dtend: DateTime<Local>,
}

impl TryFrom<ical::parser::ical::component::IcalEvent> for Event {
    type Error = String;

    fn try_from(item: ical::parser::ical::component::IcalEvent) -> Result<Self, String> {
        let summary = item.properties.iter().find(|p| p.name == "SUMMARY")
            .map(|v| v.value.clone().unwrap()).ok_or("no summary")?;
        let description: String = item.properties.iter().find(|p| p.name == "DESCRIPTION")
            .map(|v| v.value.clone().unwrap()).ok_or("no description")?;
        let location: String = item.properties.iter().find(|p| p.name == "LOCATION")
            .map(|v| v.value.clone().unwrap()).ok_or("no location")?;
        let dtstart: DateTime<Local> = item.properties.iter().find(|p| p.name == "DTSTART")
            .map(|v| DateTime::<Utc>::from_utc(
                NaiveDateTime::parse_from_str(
                    v.value.as_ref().unwrap(),
                    &"%Y%m%dT%H%M%SZ"
                ).unwrap(),
                Utc
            ).into()
        ).ok_or("no dtstart")?;
        let dtend: DateTime<Local> = item.properties.iter().find(|p| p.name == "DTEND")
            .map(|v| DateTime::<Utc>::from_utc(
                NaiveDateTime::parse_from_str(
                    v.value.as_ref().unwrap(),
                    &"%Y%m%dT%H%M%SZ"
                ).unwrap(),
                Utc
            ).into()
        ).ok_or("no dtend")?;

        Ok(
            Event {
                summary: summary,
                description: description,
                location: location,
                dtstart: dtstart,
                dtend: dtend
            }
        )
    }
}

pub fn get_events_from_calendar<B: BufRead>(calendar: B) -> Result<Vec<Event>, String> {
    let calendar: ical::parser::ical::component::IcalCalendar = ical::IcalParser::new(
        calendar
    ).next().unwrap().unwrap();

    Ok(calendar.events.into_iter().filter_map(|e| e.try_into().ok()).collect())
}