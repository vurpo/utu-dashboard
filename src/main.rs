#![feature(proc_macro_hygiene, decl_macro)]

use std::io::{
    BufReader,
    BufRead,
};

use handlebars::{Context, Handlebars, Helper, Output, RenderContext, RenderError};
use chrono::prelude::*;
use serde::{Deserialize, Serialize};

use rocket::{routes, get, State};
use rocket::http::{Status, ContentType};
use rocket::response::Content;
use rocket_contrib::serve::StaticFiles;

use std::convert::{From, Into};

mod calendar;
mod restaurants;

const TIMETABLE_TEMPLATE: &str = r#"
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="refresh" content="60">
        <link rel="stylesheet" type="text/css" href="utu-dash.css">
    </head>
    <body>
        <div id="clock">{{time}}</div>
        <div class="eventsListWrapper">
            <ul>
                {{#each events}}
                <li class="event {{this.status}}">
                    <div class="eventLineWrapper">
                        <span class="eventName">{{this.name}}</span>
                        <span class="eventTime">{{this.start_time}} - {{this.end_time}}</span>
                    </div>
                    <div class="eventLineWrapper">
                        <span class="location">{{this.location}}</span>
                        <span class="duration">{{this.duration}}</span>
                    </div>
                </li>
                {{/each}}
            </ul>
        </div>
        <div class="restaurantsWrapper">
            {{#each restaurants}}
            <div class="restaurant">
                <span class="restaurantName">{{this.name}}</span>
                <ul class="restaurantMeals">
                    {{#each this.meals}}
                        <li class="meal">
                            <span class="mealName">{{this.name}}</span>
                            <span class="mealPrice">{{this.price}}</span>
                        </li>
                    {{/each}}
                </ul>
            </div>
            {{/each}}
        </div>
    </body>
</html>
"#;

#[derive(Debug,Serialize)]
struct PageEvent {
    name: String,
    location: String,
    start_time: String,
    end_time: String,
    duration: String,
    status: &'static str
}

impl From<calendar::Event> for PageEvent {
    fn from(event: calendar::Event) -> Self {
        PageEvent {
            name: event.summary,
            location: event.location.split_whitespace().next().unwrap().to_owned(),
            start_time: event.dtstart.format(&"%H:%M").to_string(),
            end_time: event.dtend.format(&"%H:%M").to_string(),
            duration: format!("{} min", (event.dtend-event.dtstart).num_minutes()),
            status: {
                let now = Local::now();
                if event.dtstart > now {
                    "future"
                } else if event.dtstart < now && event.dtend > now {
                    "ongoing"
                } else {
                    "past"
                }
            }
        }
    }
}

#[derive(Debug,Serialize)]
struct PageInfo {
    time: String,
    events: Vec<PageEvent>,
    restaurants: Vec<restaurants::Restaurant>
}

#[get("/")]
fn handle(handlebars: State<Handlebars>) -> Result<Content<String>, Status> {
    let calendar = calendar::get_events_from_calendar(BufReader::new(
        reqwest::get("https://lukkarit.utu.fi/ical.php?hash=2F887406A12B114BD583FC8C5BA517C4AFB4C72E").unwrap()
    )).unwrap()
        .into_iter().filter(|event| event.dtstart.date() == Local::today()).collect::<Vec<_>>();

    let page_info = PageInfo {
        time: format!("{}", Local::now().format("%H:%M")),
        events: calendar.into_iter().map(Into::into).collect(),
        restaurants: restaurants::get_restaurants(&["assarin-ullakko", "galilei"]).unwrap_or(Vec::new())
    };

    handlebars.render("display", &page_info)
        .map(|r| Content(ContentType::HTML, r))
        .map_err(|_| Status::InternalServerError)
}

fn main() {
    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_string("display", TIMETABLE_TEMPLATE)
        .unwrap();

    let config = rocket::Config::build(rocket::config::Environment::Production)
        .port(8088)
        .finalize().unwrap();

    rocket::custom(config)
        .manage(handlebars)
        .mount("/", StaticFiles::from("static/"))
        .mount("/", routes![handle])
        .launch();

    //println!("{:?}", calendar.iter().filter(|event| event.dtstart.date() == Local::today() && event.dtstart > Local::now()).collect::<Vec<_>>());
}