use chrono::{
    Local,
    Datelike
};

use serde::Serialize;

#[derive(Debug,Serialize)]
pub struct Restaurant {
    name: String,
    meals: Vec<Meal>
}

#[derive(Debug,Serialize)]
pub struct Meal {
    name: String,
    price: String
}

pub fn get_restaurants(codes: &[&str]) -> Result<Vec<Restaurant>, &'static str> {
    let restaurants = reqwest::get("https://opas.utu.fi/api/restaurants/").unwrap()
        .json::<serde_json::Value>().map_err(|_|"invalid restaurant JSON")?;
    
    let restaurants = restaurants.as_array().ok_or("invalid restaurant JSON")?;

    let restaurants = restaurants.iter()
        .filter_map(|restaurant| {
            if !codes.contains(&(restaurant.get("code")?.as_str()?)) { return None }
            let name = restaurant.get("name")?.as_str()?.to_owned();
            let daynumber = Local::today().weekday().num_days_from_monday();
            let today = restaurant.get("weeklyMenus")?.get(0)?.get("dayMenus")?.get(daynumber as usize)?.get("lunches")?.as_array()?;
            let meals = today.iter().filter_map(|meal| {
                Some(Meal{
                    name: meal.get("name")?.as_str()?.to_owned(),
                    price: meal.get("price")?.as_str()?.to_owned()
                })
            }).collect::<Vec<_>>();

            Some(Restaurant{
                name: name,
                meals: meals
            })
        }).collect::<Vec<_>>();

    Ok(restaurants)
}